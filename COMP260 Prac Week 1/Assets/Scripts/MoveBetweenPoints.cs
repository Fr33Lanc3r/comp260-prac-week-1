﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour {

	//Public Parameters
	public Vector3 startPoint;
	public Vector3 endPoint;
	public float speed = 1.0f;

	//Private State
	private bool movingForward = true;

	// Use this for initialization
	void Start () {
		//move immediately to the start point
		transform.position = startPoint;
	}
	
	// Update is called once per frame
	void Update () {
		//which point are we heading for?
		Vector3 target;

		if (movingForward) {
			target = endPoint;
		} else {
			target = startPoint;
		}

		//calculate the distance to move based on speed and frame rate
		float distanceToMove = speed * Time.deltaTime;

		//calculate how far from the target
		float distanceToTarget = (target - transform.position).magnitude;
		Debug.Log ("Distance to target = " + distanceToTarget);
		Debug.Log ("Distance to move = " + distanceToMove); 

		//check if we are close to the target
		if (distanceToMove > distanceToTarget) {
			//close: move straight there and change direction
			transform.position = target;
			movingForward = !movingForward;
			Debug.Break (); //pause the game
		} else {
			//otherwise just move towards the target
			Vector3 dir = (target - transform.position).normalized;
			transform.position += dir * distanceToMove;
		}
	}
}
